#!/usr/bin/python
# by OneEyedDancer
import subprocess
import webbrowser
import sys
import random
import string

try:
    import requests
    from bs4 import BeautifulSoup
    from user_agent import generate_user_agent
    import colorama
except ModuleNotFoundError:
    if input('Нет нужных модулей, произвести их установку? [y/n]: ') == 'y':
        subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'requests', 'user_agent', 'bs4', 'colorama'])
        import requests
        from bs4 import BeautifulSoup
        from user_agent import generate_user_agent
        import colorama

        input('Установка завершена!')
    else:
        input('Выход...')
        sys.exit()


def main():
    colorama.init()
    print('''\033[31m8                          8""""8                      8""""8                                           
8     e  eeeee e   e eeeee 8      e   e eeeee eeeee    8      eeee eeeee  eeeee eeeee eeeee eeee eeeee  
8e    8  8   8 8   8   8   8eeeee 8   8 8  88   8      8eeeee 8  8 8   8  8   8 8   8 8   8 8    8   8  
88    8e 8e    8eee8   8e      88 8eee8 8   8   8e         88 8e   8eee8e 8eee8 8eee8 8eee8 8eee 8eee8e 
88    88 88 "8 88  8   88  e   88 88  8 8   8   88     e   88 88   88   8 88  8 88    88    88   88   8 
88eee 88 88ee8 88  8   88  8eee88 88  8 8eee8   88     8eee88 88e8 88   8 88  8 88    88    88ee 88   8
================================================================================@OneEyedDancer|V1.1====\033[0m''')
    with open('links.txt', 'w') as file:
        try:
            cnt = int(input('Введите количество ссылок: '))
        except ValueError:
            input('Введите число')
            return main()
        auto_open = input('Включить автоматическое открытие вкладок? [y/n]: ')

        while cnt:
            letters = string.ascii_lowercase + string.digits
            rand_string = ''.join(random.choice(letters) for i in range(6))

            url = f'https://prnt.sc/{rand_string}'
            headers = {"user-agent": generate_user_agent()}

            req = requests.get(url, headers=headers)
            soup = BeautifulSoup(req.text, "html.parser")
            try:
                image_url = soup.find('img', class_="screenshot-image").get('src')
            except AttributeError:
                continue
            if 'imgur' in image_url or "https" not in image_url:
                continue
            file.write(f'{image_url}\n')
            print(image_url)
            if auto_open == 'y':
                webbrowser.open_new_tab(image_url)
            cnt -= 1

    if input('Процесс завершен, сслыки записаны в файл. Перезапустить? [y/n]: ') == 'y':
        return main()


if __name__ == '__main__':
    main()
